# Setup Steps 

Steps to setup on a linux O/S in this case Debian on BBB

----
* Clone webapi

From a suitable location clone via 

git clone https://Ascendent@bitbucket.org/Ascendent/webapi.git

I am using /opt as my base directory 

---
* Install python3 dependencies 

pip3 install

arrow  
psutil  
cherrypy  
markupsafe  

---
* Populate support packages

The following support packages are specified in ./templates/webapi_layout.html

<link rel="stylesheet" type="text/css" href="/static/bootstrap/css/bootstrap.min.css" />  
<script src="/static/jquery.min.js"></script>  
<script src="/static/popper.min.js"></script>  
<script src="/static/bootstrap/js/bootstrap.min.js"></script>  

cd to ./static and add the following packages 

wget https://github.com/twbs/bootstrap/releases/download/v4.1.0/bootstrap-4.1.0-dist.zip
Then unzip  

wget https://code.jquery.com/jquery-3.3.1.min.js   
wget https://unpkg.com/popper.js/dist/umd/popper.min.js  

Use symbolic links to the generic dir or file. For example. 

ln -s bootstrap-4.1.0 bootstrap  
ln -s jquery-3.3.1.min.js jquery.min.js     

---
* Run app

You may need to chmod webapi.py 

chmod 744 webapi.py 

Then 

./webapi.py 

Cherrypy is setup in development mode so it will restart itself whenever you change a file in the working
directory. On a BBB this restart is slow so have patience. 

I use winscp to develop and geany or notepad++ on Windows. On Linux I mount the BBB and use geany. 
There are other options, use whatever tools work for you. 

Add any comments or let me know what I missed. 


